import Vue from 'vue'
import Router from 'vue-router'
import Customer from './pages/customer/customer.vue';
import SalesList from './pages/saleslist/saleslist.vue';
import Payment from './pages/payment/payment.vue';
import Login from './pages/login/login.vue';
import Menu from './pages/login/menu.vue';

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/menu',
            name: 'menu',
            component: Menu
        }, {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/',
            name: 'customer',
            component: Customer
        },
        {
            path: '/saleslist/:id', // :id를 작성하지 않아도 화면은 찍히지만 url주소에 id가 포함이 안되네...
            name: 'saleslist',
            component: SalesList
        },
        {
            path: '/payment/:id',
            name: 'payment',
            component: Payment
        },
        {
            path: '/paymentEdit/:id/:smIdx',
            name: 'paymentEdit',
            component: Payment
        }
    ]
})