import Constant from '../../Constant'
import Conf from '../../Config'
import axios from 'axios'

const state = {
    CustomerList: []
}

// getters
const getters = {
    getCustomerList: (state) => {
        // console.log("here!"); // 새로고침시 2번 호출되네...?
        return state.CustomerList;
    }
}

// mutations
const mutations = {
    [Constant.CUSTOMER_LIST]: (state, payload) => {
        state.CustomerList = payload.CustomerList;
    }
}

// actions
const actions = {
    [Constant.CUSTOMER_LIST]: (store, payload) => {
        if (payload.SearchValue == " ") {
            payload.SearchValue = "true";
        }

        axios.get(Conf.CUSTOMER_LIST + "/" + payload.page + "/" + payload.SearchValue)
            .then((response) => {
                store.commit(Constant.CUSTOMER_LIST, { CustomerList: response.data });
            });
    },
    [Constant.CUSTOMER_ADD]: (store, payload) => {
        axios.post(Conf.CUSTOMER_ADD, { CustName: payload.Name, Hp: payload.Hp })
            .then((response) => {
                store.dispatch(Constant.CUSTOMER_LIST, { SearchValue: ' ', page: 1 }); // 추가한걸 가져오는것 ㄴㄴ, state에 하나를 추가시키자! // 아니다 state에 custIdx를 가져올 수 없어서 전체를 가져와야 함
                // 음... 추가할때 return을 전체 데이터로 하는걸로 바꾸자!
            })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}