import Conf from '../../Config'
import axios from 'axios'

const state = {
    Authorization: ''
}

// getters
const getters = {
    getAuthorization: (state) => {
        return state.Authorization;
    }
}

// mutations
const mutations = {
    ["ADD_AUTHTOKEN"]: (state, payload) => {
        state.Authorization = payload.Authorization;
    }
}

// actions
const actions = {
    ["GET_AUTHTOKEN"]: (store, payload) => {
        axios.post("https://localhost:44365/api/v1/Users/authenticate", {
            UserName: payload.userId,
            Password: payload.userPwd
        }).then((response) => {
            console.log(response.data);
            store.commit("ADD_AUTHTOKEN", { Authorization: response.data });
        });
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}