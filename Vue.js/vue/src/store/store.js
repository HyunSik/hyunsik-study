import Vue from 'vue';
import Vuex from 'vuex';
import page_customer from './modules/page-customer'
import common from './modules/common'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        m1: page_customer,
        common: common
    }
})

export default store;