var BASE_URL = "https://localhost:44316/api";

export default {
    CUSTOMER_LIST: BASE_URL + "/Customer",
    CUSTOMER_ADD: BASE_URL + "/Customer/Add",
    CUSTOMER_COUNT: BASE_URL + "/Customer/Count",
    SALES_LIST: BASE_URL + "/Sales",
    SERVICECLASS_LIST: BASE_URL + "/Payment/ServiceClass",
    SERVICE_LIST: BASE_URL + "/Payment/Service",
    SALES_ADD: BASE_URL + "/Payment",
    SALES_EDIT_VIEW: BASE_URL + "/Payment/EditView",
    SALES_EDIT: BASE_URL + "/Payment/Edit"
}